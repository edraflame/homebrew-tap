class Osti < Formula
  desc "Hosts file building"
  homepage "https://gitlab.com/enricomonese/osti"
  url "https://gitlab.com/EnricoMonese/osti/-/archive/v1.1.3/osti-v1.1.3.tar.gz"
  sha256 "8dd5eefa97602129dcab3aea10d8e16750dc01967edfeab33906db96fdc7fbeb"
  version "1.1.3"
  head "https://gitlab.com/enricomonese/osti.git"

  depends_on "crystal-lang" => :build

  def install
    system "make"
    bin.install "bin/osti"
  end
end